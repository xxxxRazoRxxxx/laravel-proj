<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products/', 'ProductController@viewAll');

Route::get('/product/{id?}', 'ProductController@view');

Route::get('/products_sort/', 'ProductController@viewAllSort');
Route::get('/products_sort2/', 'ProductController@viewAllSort2');

Route::get('/product_create/', 'ProductController@create');
