<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function descriptions()
    {
        return $this->hasMany('App\ProductDescription')->select(['name', 'description', 'product_id']);
    }
}
