<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductDescription;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\DocBlock\Description;

class ProductController extends Controller
{
    protected $items_per_page = 10;

    protected $fields = [
        'products.id',
        'product_code',
        'price',
        'product_descriptions.name as name',
    ];

    /**
     * Gets products with next load of descriptions
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAll()
    {
        $product_list = Product::get();

        $product_list->load(['descriptions' => function($query) {
            $query->where('lang_code', '=', 'ru');
        }]);

        return view('products', ['products' => $product_list]);
    }

    /**
     * Gets products with sorting params
     * Example: ?page=2&sl=ru&sort_by=name
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAllSort(Request $request)
    {
        $params = [
            'sort_by'   => !empty($request->get('sort_by')) ? $request->get('sort_by') : 'name',
            'sort_type' =>  !empty($request->get('sort_type')) ? $request->get('sort_type') : 'asc',
            'page'      => !empty($request->get('page')) ? $request->get('page') : 1,
            'sl'        => !empty($request->get('sl')) ? $request->get('sl') : 'en',
        ];

        $product_list = Product::select($this->fields)
            ->join('product_descriptions', function ($join) use ($params) {
                $join->on('products.id', '=', 'product_descriptions.product_id')
                    ->where('product_descriptions.lang_code', '=', $params['sl']);
            })
            ->orderBy($params['sort_by'], $params['sort_type'])
            ->offset(($params['page'] - 1) * $this->items_per_page)
            ->limit($this->items_per_page)
            ->get();

        return view('products', ['products' => $product_list]);
    }


    /**
     * Gets products with description with sorting params
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAllSort2(Request $request)
    {
        $params = [
            'sort_by'   => !empty($request->get('sort_by')) ? $request->get('sort_by') : 'name',
            'sort_type' => !empty($request->get('sort_type')) ? $request->get('sort_type') : 'asc',
            'page'      => !empty($request->get('page')) ? $request->get('page') : 1,
            'sl'        => !empty($request->get('sl')) ? $request->get('sl') : 'en',
        ];

        $product_list = Product::with(['descriptions' => function($query) {
            $query->where('lang_code', '=', 'ru');
        }])
            ->select($this->fields)
            ->join('product_descriptions', function ($join) use ($params) {
                $join->on('products.id', '=', 'product_descriptions.product_id')
                    ->where('product_descriptions.lang_code', '=', $params['sl']);
            })
            ->orderBy($params['sort_by'], $params['sort_type'])
            ->offset(($params['page'] - 1) * $this->items_per_page)
            ->limit($this->items_per_page)
            ->get();

        return view('products', ['products' => $product_list]);
    }

    /**
     * Gets product data by ID
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id = 1)
    {
        $product_list = Product::with(['descriptions' => function($query) {
            $query->where('lang_code', '=', 'ru');
        }])
            ->where('id', '=', $id)
            ->get();

        return view('products', ['products' => $product_list]);
    }

    /**
     * Creates product with name Яблоко
     *
     * @param Request $request
     */
    public function create(Request $request)
    {
        $product = new Product();
        $product->price = 123.59;
        $product->product_code = 10020;
        $product->save();

        $description = new ProductDescription();
        $description->name = 'Яблоко';
        $description->description = 'Очень вкусное';
        $description->lang_code = 'ru';
        $product->descriptions()->save($description);

        $description = new ProductDescription();
        $description->name = 'Apple';
        $description->description = 'Very taste';
        $description->lang_code = 'en';
        $product->descriptions()->save($description);
    }
}
